"use strict";

const filterBy = (arr, dataTypes) => arr.filter(item => typeof item !== dataTypes);

console.log(filterBy(['hello', 'world', 23, '23', null], 'number'));
