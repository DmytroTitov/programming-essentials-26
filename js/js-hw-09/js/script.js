"use strict";

const tabsRef = document.querySelector('.js-tabs');

tabsRef.addEventListener('click', e => {
    const tabsItemsRef = document.querySelectorAll('.js-tabs > li');
    const listItems = document.querySelectorAll('.tabs-content > li');
    const cursorTarget = e.target;

    tabsItemsRef.forEach(item => {
        if (item.classList.contains('active')) {
            item.classList.remove('active');
        }
    })

    cursorTarget.classList.add('active');

    listItems.forEach(item => {
        item.classList.remove('content-active');
        item.classList.add('hidden');
        if (item.dataset.description.toLowerCase() === cursorTarget.textContent.toLowerCase()) {
            item.classList.remove('hidden');
            item.classList.add('content-active');
        }
    })
});