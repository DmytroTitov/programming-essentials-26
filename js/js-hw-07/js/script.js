"use strict";

const exampleArr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const exampleArr2 = ["1", "2", "3", "sea", "user", 23];
// const exampleArr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

// -------------------------Option with createElement---------------------------
// const createList = (arr, parentEL = "body") => {
//
//     const parentEl = document.querySelector(parentEL);
//
//     const createMarkup = (item) => {
//         const elementRef = document.createElement('li');
//         elementRef.textContent = item;
//         return elementRef;
//     }
//
//     const listItems = arr.map(item => createMarkup(item));
//
//     parentEl.prepend(...listItems);
//
//
// }
//
// createList(exampleArr1);

// const createList = (arr, parentEL = "body") => {
//     const insertElement = (item) => {
//         document.querySelector(parentEL).insertAdjacentHTML('afterbegin', `<li>${item}</li>`);
//     }
//     arr.map(item => insertElement(item));
// }
//
// createList(exampleArr1, 'ul');


const createList = (arr, parentEL = "body") => {
    const insertElement = (item) => document.querySelector(parentEL).insertAdjacentHTML('afterbegin', `<li>${item}</li>`);

    arr.map(item => insertElement(item));
}

createList(exampleArr1, 'ul');
