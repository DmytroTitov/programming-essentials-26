"use strict"

const $newsSectionRef = $('.news');
const $topBtn = $('<button>Top</button>');
const $hideFadeBtn = $('<button>Hide/Fade</button>');

$($topBtn).css({
    'position': 'fixed',
    'bottom': '20px',
    'right': '0',
    'width': '70px',
    'height': '70px',
    'font-size': '12px',
    'color': 'black',
    'background-color': 'green',
    'outline': 'none',
    'cursor': 'pointer',
    'border': '1px solid black',
    'border-radius': '50%',
});

$($hideFadeBtn).css({
    // 'position': 'absolute',
    // 'bottom': '-400px',
    // 'left': '50%',
    // 'transform': 'translateX(-50%)',
    'display': 'block',
    'width': '150px',
    'height': '50px',
    'margin': '0 auto',
    'font-size': '12px',
    'color': 'black',
    'background-color': 'gold',
    'outline': 'none',
    'cursor': 'pointer',
    'border': '1px solid black',
    'border-radius': '5px',
});

$topBtn.click(() => {
    $('html, body').animate({scrollTop: 0}, 1000);
});

$hideFadeBtn.click(() => {
    $newsSectionRef.slideToggle();
});

$($newsSectionRef).after($hideFadeBtn);

const $page = $('html, body');
$('a[href*="#"]').click(function (e) {
    $page.animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
    e.preventDefault();
    return false;
});

$(document.body).prepend($topBtn);

$topBtn.hide();

$(window).scroll(function() {
    if ($(window).scrollTop() > $(window).height()) {
        $topBtn.show(300);
    } else {
        $topBtn.hide(300);
    }
});

