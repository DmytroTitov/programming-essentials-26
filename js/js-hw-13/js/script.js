"use strict";

const switchBtnRef = document.querySelector('.js-switch');

function setTheme(themeName) {
    localStorage.setItem('theme', themeName);
    document.documentElement.className = themeName;
}

function toggleTheme() {
    if (localStorage.getItem('theme') === 'theme-dark') {
        setTheme('theme-light');
    } else {
        setTheme('theme-dark');
    }
}

document.addEventListener("DOMContentLoaded", () => {
    document.documentElement.className = localStorage.getItem('theme');
});


switchBtnRef.addEventListener('click', toggleTheme);