## Вопрос 1
>Опишите своими словами, как Вы понимаете, что такое обработчик событий.

**Ответ:**
Обработчик событий это один из интерфейсов веб API, который является объектом с возможностью обработать определенное событие. Регистрируется с помощью метода addEventListener() и вызывается на EventTarget. В результате совершения определенных действий пользователя, которые отслеживаются, выполняет определенную функцию (коллбек/метод handleEvent).