"use strict";

const inputRef = document.querySelector('.js-input');
const alertMessage = document.createElement('span');
const priceMessage = document.createElement('span');
const priceButton = document.createElement('button');

inputRef.addEventListener("focus", (e) => {
    const targetElem = e.target;
    targetElem.value = '';
    targetElem.style.borderColor = "green";
    targetElem.style.color = "black";
    alertMessage.remove();
    priceMessage.remove();
    priceButton.remove();
})


inputRef.addEventListener("blur", (e) => {

    const formRef = document.querySelector('.js-form');
    const targetElem = e.target;
    const userValue = targetElem.value;

    if (userValue < 0 || userValue === "") {
        alertMessage.textContent = "Please enter correct price";
        targetElem.style.color = "red";
        targetElem.style.borderColor = "red";
        alertMessage.style.cssText = `
        position: absolute;
        bottom: 40%;
        color: red;
        font-size: 20px;
        font-weight: bold;
        `;

        formRef.after(alertMessage);
    } else {

        priceMessage.textContent = `Текущая цена: ${userValue}`;
        priceButton.textContent = 'X';

        targetElem.style.borderColor = "transparent";
        targetElem.style.color = "green";
        priceMessage.style.cssText = `
        position: absolute;
        top: 38%;
        margin-bottom: 15px;
        background-color: tomato;
        color: white;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 15px;
        padding-right: 15px;
        border-width: 1px;
        border-style: solid;
        border-color: #333;
         border-radius: 25px;        
        `;
        priceButton.style.cssText = `
        width: 25px;
        height: 25px;
        margin-left: 15px;
        background-color: black;
        color: white;
        border-width: 1px;
        border-style: solid;
        border-color: #333;
        border-radius: 50%;
        outline: none; 
        `;

        priceMessage.append(priceButton);
        formRef.before(priceMessage);

    }
})

priceButton.addEventListener('click', () => {
    inputRef.value = '';
    priceMessage.remove();
    priceButton.remove();
})