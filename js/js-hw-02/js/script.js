"use strict";

let userNumber = prompt("Enter your number, please");

while (Number.isInteger(userNumber)) {
    userNumber = prompt("Your number isn't integer. Enter your number again, please");
}

if (userNumber < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 0; i <= userNumber; i += 5) {
        if (i % 5 === 0 && i !== 0) {
            console.log(i);
        }
    }
}


//Additional

let m = prompt("Enter min number, please");

while (Number.isInteger(m) || Math.sign(m) === -1) {
    m = prompt("Your number isn't integer or positive. Enter your min number again, please");
}

let n = prompt("Enter max number, please");

while (Number.isInteger(n)) {
    n = prompt("Your number isn't integer. Enter your min number again, please");
}


for (let i = m; i <= n; i++) {
    let count = 0;
    for (let j = 1; j <= i; j++) {
        if (i % j) {
            continue;
        }
        count += 1;
    }
    if (count === 2) {
        console.log(i);
    }
}
