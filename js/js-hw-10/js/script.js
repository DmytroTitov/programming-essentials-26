"use strict";

const formRef = document.querySelector('.password-form');
const firstInput = document.querySelector('.js-first-input');
const secondInput = document.querySelector('.js-second-input');
const firstIcon = document.querySelector('.fas[data-icon="first-icon"]');
const secondIcon = document.querySelector('.fas[data-icon="second-icon"]');
const incorrectMessage = document.createElement('span');

const showPassword = (input, icon) => {
    if (input.getAttribute('type') === 'password') {
        input.removeAttribute('type');
        input.setAttribute('type', 'text');
        icon.className = 'fas fa-eye-slash icon-password';
    } else {
        input.removeAttribute('type');
        input.setAttribute('type', 'password');
        icon.className = 'fas fa-eye icon-password';
    }
}

const listenPassword = e => {
    if (e.target === firstIcon && firstInput.value !== '') {
        showPassword(firstInput, firstIcon);
    }
    if (e.target === secondIcon && secondInput.value !== '') {
        showPassword(secondInput, secondIcon);
    }

    if (e.target === firstInput || e.target === secondInput) {
        incorrectMessage.remove()
    }
}

formRef.addEventListener('click', listenPassword);

formRef.addEventListener('submit', e => {
    e.preventDefault();

    if (firstInput.value === secondInput.value) {
        alert('You are welcome');
    } else {

        incorrectMessage.textContent = "Нужно ввести одинаковые значения";
        incorrectMessage.style.color = "red";
        incorrectMessage.style.borderColor = "red";
        incorrectMessage.style.cssText = `
           position: absolute;
           bottom: 7px;
           left: 0;
           color: red;
           font-size: 12px;
           font-weight: bold;
           }`;

        secondIcon.after(incorrectMessage);
    }
})

