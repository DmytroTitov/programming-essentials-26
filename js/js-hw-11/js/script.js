"use strict";

const btnsRef = document.querySelectorAll('.btn');

window.addEventListener('keydown', e => {

    btnsRef.forEach(item => {
        if (e.key.toLowerCase() === item.textContent.toLowerCase()) {
            item.style.backgroundColor = 'blue';
        } else {
            item.style.backgroundColor = 'black';
        }
    })
})