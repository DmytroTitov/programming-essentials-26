"use strict";

const getUserValue = (count = 'first') => {
    let value = prompt(`Enter your ${count} number, please`);

    while (value.length === 0 || isNaN(+value)) {
        value = prompt(`You didn't entered correct number, enter your ${count} number again, please`);
    }

    return value;
}

const calculateOperation = (firstNumber, secondNumber) => {
    const operation = prompt("Enter your operation please", "'+' or '-', or '*', or '/'");

    switch (operation) {
        case "+":
            return Number(firstNumber) + Number(secondNumber);
        case "-":
            return firstNumber - secondNumber;
        case "*":
            return firstNumber * secondNumber;
        case "/":
            return firstNumber / secondNumber;
        default:
            return "Your operation is incorrect"
    }
}

const countingResult = () => calculateOperation(getUserValue(), getUserValue('second'));

console.log(countingResult());