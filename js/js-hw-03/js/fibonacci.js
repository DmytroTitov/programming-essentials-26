"use strict";

const getUserValue = () => {
    let value = prompt(`Enter your number, please`);

    while (value.length === 0 || isNaN(+value)) {
        value = prompt(`You didn't entered correct number, enter your number again, please`, value);
    }

    return value;
}

function fib(n) {
    if (n >= 0) {
        let a = 1;
        let b = 1;
        for (let i = 3; i <= n; i++) {
            let c = a + b;
            a = b;
            b = c;
        }
        return b;
    } else {
        let a = -1;
        let b = -1;
        for (let i = -3; i >= n; i--) {
            let c = a + b;
            a = b;
            b = c;
        }
        return b;
    }
}

console.log(fib(getUserValue()));