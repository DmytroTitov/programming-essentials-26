"use strict";

const getUserValue = () => {
    let value = prompt(`Enter your number, please`);

    while (value.length === 0 || isNaN(+value)) {
        value = prompt(`You didn't entered correct number, enter your number again, please`, value);
    }

    return value;
}

const factorial = (value) => {
    return (value !== 1) ? value * factorial(value - 1) : 1;
}

console.log(factorial(getUserValue()));