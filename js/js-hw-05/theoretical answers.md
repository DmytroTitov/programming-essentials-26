## Вопрос 1
>Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

**Ответ:**
Экранирование это добавление левого слеша (\) перед спец. символами, которые существуют в JS (точка, кавычки, слеш и т.д.), для нормального отображения или поиска таких спецсимволов как обычных символов. В случае, если спец. символ не экранировать, то возможны ошибки или нежелательные последствия в коде, т.к. интерпретатор может воспринять такой символ как часть команды.