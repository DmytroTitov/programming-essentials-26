"use strict";

function createNewUser(userName, userSurname, userBirthday) {
    const newUser = {
        userName,
        userSurname,
        userBirthday,
        getLogin: function () {
            return this.userName[0].toLowerCase() + this.userSurname.toLowerCase();
        },
        getNormalDate: function () {
            const dateAr = this.userBirthday.split('.');
            return new Date(`${dateAr[2]}.${dateAr[1]}.${dateAr[0]}`);
        },
        getAge: function () {
            const date = this.getNormalDate();
            const dateAgo = new Date(date).setHours(0);
            const dateNow = new Date().setHours(0);
            return Math.floor((dateNow - dateAgo) / (24 * 3600 * 365.25 * 1000));
        },
        getPassword: function () {
            const date = this.getNormalDate();
            return this.userName[0].toUpperCase() + this.userSurname.toLowerCase() + date.getFullYear()
        }
    };
    return newUser;
}

const myNewUser = createNewUser(prompt("Enter your name, please"), prompt('Enter your surname, please'), prompt("Enter your birthday, please", "dd.mm.yyyy"));

console.log(myNewUser.getLogin());
console.log(myNewUser.getAge());
console.log(myNewUser.getPassword());

console.log(myNewUser);




