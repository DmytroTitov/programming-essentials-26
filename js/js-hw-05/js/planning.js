"use strict";

function isInDeadline(speedArr, backlogArr, deadlineDate) {
    const totalSpeed = speedArr.reduce((acc, currentValue) => acc + currentValue);
    const totalTasks = backlogArr.reduce((acc, currentValue) => acc + currentValue);
    const totalDays = totalTasks / totalSpeed;

    const normalisedDeadlineDate = function () {
        const dateAr = deadlineDate.split('.');
        return new Date(`${dateAr[2]}.${dateAr[1]}.${dateAr[0]}`);
    }

    let end = normalisedDeadlineDate();
    let start = new Date();

    const getDatesBetween = (startDate, endDate) => {
        const dates = [];

        let currentDate = new Date(
            startDate.getFullYear(),
            startDate.getMonth(),
            startDate.getDate()
        );

        while (currentDate <= endDate) {

            if (currentDate.getDay() < 6 && currentDate.getDay() > 0) {
                dates.push(currentDate.getDay());
            }

            currentDate = new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                currentDate.getDate() + 1,
            );
        }
        return dates.length;
    };

    const dates = getDatesBetween(start, end);

    if (dates > totalDays) {
        return `Все задачи будут успешно выполнены за ${dates - Math.ceil(totalDays)} дней до наступления дедлайна!`
    } else {
        return `Команде разработчиков придется потратить дополнительно ${Math.ceil((totalDays - dates) * 8)} часов после дедлайна, чтобы выполнить все задачи в беклоге`
    }
}

console.log(isInDeadline([4, 5, 3, 6, 1], [25, 10, 11, 3, 5, 6, 7], "01.11.2020"));



