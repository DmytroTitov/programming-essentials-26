"use strict";

const randomObj = {
    a: 5,
    b: {g: 8, y: 9, t: {q: 48}},
    x: 47,
    l: {f: 85, p: {u: 89, m: 7}, s: 71},
    r: {h: 9, a: 'test', s: 'test2'}
};

console.log('RandomObj before cloning: ', randomObj);

function deepClone(obj) {
    const cloneObj = {};
    for (const i in obj) {
        if (obj[i] instanceof Object) {
            cloneObj[i] = deepClone(cloneObj[i]);
            continue;
        }
        cloneObj[i] = obj[i];
    }
    return cloneObj;
}

const clonedRandomObj = deepClone(randomObj);

console.log('RandomObj object after cloning: ', randomObj);
console.log('ClonedRandomObj: ', clonedRandomObj);