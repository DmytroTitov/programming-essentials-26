"use strict";

function createNewUser() {
    const newUser = {};

    Object.defineProperties(newUser, {
            "firstName": {
                value: prompt("Enter your first name, please"),
                configurable: true,
            },
            "lastName": {
                value: prompt("Enter your last name, please"),
                configurable: true,
            },
            "setFirstName": {
                set(value) {
                    Object.defineProperty(this, "firstName", {value});
                }
            },
            "setLastName": {
                set(value) {
                    Object.defineProperty(this, "lastName", {value});
                },
            },
            "getLogin": {
                value: () => newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase(),
            },
        }
    )
    return newUser;
}

const myNewUser = createNewUser();

console.log(myNewUser.getLogin());

console.log(myNewUser);

myNewUser.setFirstName = "Pakito";
myNewUser.setLastName = "Gonsales";

console.log(myNewUser);