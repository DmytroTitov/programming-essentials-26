"use strict";

const student = {
    name: null,
    lastName: null,
}

student.name = prompt("Enter your name, please");
student.lastName = prompt("Enter your last name, please");

const tabel = {};

while (true) {
    const subject = prompt("Enter your subject, please");

    if (!subject) {
        break;
    }
    const grade = +prompt("Enter your grade, please");

    tabel[subject] = grade;
}

student.tabel = tabel;

let badGradesCounter = 0;

for (let grades in student) {
    if (student[grades] < 4) {
        badGradesCounter++;
    }
}

if (badGradesCounter === 0){
    console.log("Студент переведен на следующий курс");
}

let gradesTotal = 0;
let gradesCounter = 0;
let averageGrade = 0;

for (let grades in tabel) {
    gradesCounter++;
    gradesTotal += tabel[grades];

    averageGrade = gradesTotal / gradesCounter;
}

if (averageGrade > 7) {
    console.log("Студенту назначена стипендия");
}


