"use strict";

const currentImage = document.querySelector('.image-to-show');

const stopBtn = document.querySelector('.stop-btn');
const startBtn = document.querySelector('.start-btn');

const images = ["./img/1.jpg", "./img/2.jpg", "./img/3.JPG", "./img/4.png"];

let index = 1;

let isFirstClick = true;

const playImages = function () {
    if (index === images.length) {
        index = 0;
    }

    currentImage.src = images[index++];
}

let interval = setInterval(playImages, 3000);

stopBtn.addEventListener('click', () => {
    clearInterval(interval);
    isFirstClick = false;
})

startBtn.addEventListener('click', () => {
    if (!isFirstClick) {
        interval = setInterval(playImages, 3000);
    }

    isFirstClick = true;
})