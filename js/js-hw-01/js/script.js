"use strict";

let userAge = prompt("Enter your your age, please");
let userName = prompt("Enter your name, please");
const notAllowedMessage = "You are not allowed to visit this website";
const continueAllowedMessage = "Are you sure you want to continue?";
const welcomeUserMessage = `Welcome, ${userName}`;


if (userName.length === 0 || (isNaN(+userAge))) {
    userName = prompt("Please repeat and enter your name again", userName);
    userAge = prompt("Please repeat and enter your age again", userAge);
    if (userAge < 18) {
        alert(notAllowedMessage);
    } else if (userAge <= 22) {
        const userAgeAnswer = confirm(continueAllowedMessage);
        userAgeAnswer ? alert(welcomeUserMessage) : alert(notAllowedMessage);
    } else {
        alert(welcomeUserMessage);
    }
} else  {
    if (userAge < 18) {
        alert(notAllowedMessage);
    } else if (userAge <= 22) {
        const userAgeAnswer = confirm(continueAllowedMessage);
        userAgeAnswer ? alert(welcomeUserMessage) : alert(notAllowedMessage);
    } else {
        alert(welcomeUserMessage);
    }
}


